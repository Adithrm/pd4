#include<stdio.h>
int swap(int *,int *);
int main()
{
int a,b;
printf("Enter the two numbers to be swapped\n");
scanf("%d\t%d",&a,&b);
swap(&a,&b);
printf("The two numbers after swapping are : %d and %d",a,b);
return 0;
} 
int swap(int *a,int *b)
{
int temp;
temp=*a;
*a=*b;
*b=temp;
return 0;
}
